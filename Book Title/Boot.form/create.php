<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Book Title</h2>
    <form>
        <div class="form-group">
            <label for="Book title">Book title:</label>
            <input type="Book title" class="input-group"   style="background-color:#b9def0 " id="book title" placeholder="Enter book title">
        </div>
        <div class="form-group">
            <label for="Author Name">Author name:</label>
            <input type="Author name" class="input-group" style="background-color:#b9def0 " id="Author Name" placeholder="Enter author name">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>

</body>
</html>
